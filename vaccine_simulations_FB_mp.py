"""
This is a module to run vaccine optimisation simulations on a facebook
network and store the results in a SQLite database.
"""
import operator
from epidemic import Epidemic as ecr
import vaccine_optimisation as vo
import numpy as np
import multiprocessing as mp
import sys
import traceback
import sqlite3
import itertools
import networkx as nx


def main_simulations(network_fname, virus_params, vaccine_params, ga_params,
                     db_filename):
    """
    For each given parameter setting, run the required number of simluations,
    searching for the approximate optimal solution via the Genetic Algorithm
    heuristic.
    """
    network = make_network(network_fname)
    threshold, recovery_rate, complex_contag = virus_params[:]

    cr = ecr(threshold, recovery_rate)

    vaccine_limit, stop_time = vaccine_params[:]

    # Genetic Algorithm Parameters.
    individ_num, crossover, tournament_size, top_fittest, ensemble_size, \
        generations, convergence_number = ga_params[:]

    node_number = network.number_of_nodes()
    children = vo.rand_individuals(node_number, vaccine_limit, individ_num)
    best_fitness = node_number
    best_tuple = ()
    g = 0
    converge_check = 0
    max_converge = 0
    while (g <= generations) and (converge_check <= convergence_number):
        # print '--'*20
        # print 'generation ', g
        individuals = children
        fitness_tuples = []
        # for each individual, run the simulation and get the number of
        # infected.
        for i, indy in enumerate(individuals):

            mean, std = run_emsemble(cr, network, indy, complex_contag,
                                     ensemble_size)

            if mean < best_fitness:
                best_fitness = mean
                best_tuple = (mean, std)
                best_individual = indy
                converge_check = 0
            elif mean == best_fitness:
                converge_check += 1
                if converge_check > max_converge:
                    max_converge = converge_check
                # print '**** New convergence **** ', converge_check
            fitness_tuples.append((indy, mean, std))

        # print 'Best fitness of generation: ', best_tuple
        # sort for fitness
        sorted_fitness = sorted(fitness_tuples, key=operator.itemgetter(1))
        fitness_arr = np.zeros((node_number, individ_num))

        for tup_ind, tup in enumerate(sorted_fitness):
            fitness_arr[:, tup_ind] = tup[0]
        children = vo.new_generation(fitness_arr, top_fittest, vaccine_limit,
                                     tournament_size, crossover)
        g += 1

    print 'best_tuple: ', best_tuple

    if converge_check == max_converge:
        stop_type = 'converged'
    else:
        stop_type = 'generations'
    sim_results = [best_tuple[0], best_tuple[1], best_individual, stop_type]

    if db_filename is not None:
        db_connection = sqlite3.connect(db_filename)
        cursor = db_connection.cursor()
        insert_sim(cursor, virus_params, vaccine_params, ga_params,
                   sim_results)
        db_connection.commit()
        cursor.close
        db_connection.close()
    else:
        return sim_results


def run_emsemble(cr, network, indy, complex_contag, ensemble_size):
    """
    Function employs multiprocessing module to map jobs to different cores,
    collate results.
    """
    arg_tuple = (cr, network, indy, complex_contag)
    arg_tuple_list = [arg_tuple for n in xrange(ensemble_size)]
    pool_num = mp.cpu_count()
    pool = mp.Pool(pool_num)
    try:
        temp_fit_vec = pool.map(one_run, arg_tuple_list)
        pool.close()
        pool.join()
        print "".join(traceback.format_exception(*sys.exc_info()))

    except:
        # Didn't work? Put all exception text into an exception and raise that
        raise Exception("".join(traceback.format_exception(*sys.exc_info())))
    mean = np.mean(temp_fit_vec)
    std = np.std(temp_fit_vec)
    return mean, std


def one_run(arg_tuple):
    """
    Conduct a single simulation given all the simulation parameters and return
    the results.
    """
    cr, network, indy, complex_contag = arg_tuple[:]
#             init network
    cr.init_network_premade(network)
#             immunise network
    cr.immunise_nodes(indy)
    # find the number of initially immunized
    init_immunized_nodes = np.sum(indy)
#             seed network
    if complex_contag:
        cr.seed_neighborhood()
    else:
        cr.seed_simple()
#             get the fitness.
    stop_time, results = cr.simulate_cont(complex_contag)
    return results[-1] - init_immunized_nodes


def parameter_scan_grid(pool_num, network_fname, thresh_list, recov_list,
                        complex_contag, vaccine_lim_list, stop_time, ga_params,
                        db_filename, create_db):
    """
    Contstuct a job list of arg tuples which contain parameters to simulate.
    Pass that job list to the shell script which does the simlations over
    multiple cores.
    """
    print 'Running...'
    # Genetic Algorithm Parameters.
    individ_num, crossover, tourn_size, top_fit, ensem_size, \
        gens, converge_num = ga_params[:]

    # setup db first
    if create_db:
        create_database(db_filename)

    # make job list
    param_tuples = itertools.product(*[[network_fname], thresh_list,
                                     recov_list,
                                     [complex_contag], vaccine_lim_list,
                                     [stop_time], [individ_num], [crossover],
                                     [tourn_size], [top_fit], [ensem_size],
                                     [gens], [converge_num], [db_filename]])

    # results = []
    for arg_tuple in param_tuples:
        print 'running: ', arg_tuple
        shell_script(arg_tuple)
        print 'Completed'

        # sim_results = shell_script(arg_tuple)
        # results.append(sim_results)


def shell_script(arg_tuple):
    """
    A function to ingest an arg tuple that contains the parameters for the
    simulations to take place. Needed because it is a simple way to give the
    multiprocessing module methods many parameters.
    """
    [network_fname, threshold, recovery_rate, complex_contag, vaccine_limit,
     stop_time, individ_num, crossover, tourn_size, top_fit, ensem_size, gens,
     converge_num, db_filename] = arg_tuple[:]

    virus_params = [threshold, recovery_rate, complex_contag]

    vaccine_params = [vaccine_limit, stop_time]

    # Genetic Algorithm Parameters.
    ga_params = [individ_num, crossover, tourn_size, top_fit,
                 ensem_size, gens, converge_num]

    sim_results = main_simulations(network_fname, virus_params, vaccine_params,
                                   ga_params, db_filename)
    return sim_results


def vector_to_string(vec):
    """
    Used to convert a vector to a string for inputting to the SQLite DB
    """
    vec_str = ''
    for val in vec:
        vec_str = vec_str + str(int(val)) + ','
    return vec_str


def create_database(db_filename):
    """Set up the initial DB."""
    # Initialize DB
    db_connection = sqlite3.connect(db_filename)
    cursor = db_connection.cursor()
    cursor.execute('''
    CREATE TABLE simulations(ID INTEGER PRIMARY KEY, Infection REAL,
    RecovRate REAL, ComplexContag INTEGER, VaccLim INTEGER, StopTime INTEGER,
    CrossOver REAL, Gens INTEGER, Converge INTEGER, IndividNum INTEGER,
    TopFittest INTEGER, TourSize INTEGER, EnsemSize INTEGER, BestMean REAL,
    BestSTD REAL, BestIndivid TEXT, StopType TEXT)''')
    db_connection.commit()
    cursor.close
    db_connection.close()


def insert_sim(cursor, virus_params, vaccine_params, ga_params, sim_results):
    """
    This adds the init parameters and setup, with the average results and
    statistics of a run to a given database for statistical purposes, later.
    """

    threshold, recovery_rate, complex_contag = virus_params[:]
    vaccine_limit, stop_time = vaccine_params[:]
    individ_num, crossover, tourn_size, top_fittest, ensem_size, \
        gens, converge_num = ga_params[:]
    best_mean, best_std, best_individ, stop_type = sim_results[:]
    best_individ_str = vector_to_string(best_individ)

    cursor.execute("""INSERT INTO simulations(Infection, RecovRate,
                   ComplexContag, VaccLim, StopTime, CrossOver, Gens, Converge,
                   IndividNum, TopFittest, TourSize, EnsemSize, BestMean,
                   BestSTD, BestIndivid, StopType)
                   VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""",
                   (threshold, recovery_rate, complex_contag, vaccine_limit,
                    stop_time, crossover, gens, converge_num, individ_num,
                    top_fittest, tourn_size, ensem_size, best_mean, best_std,
                    best_individ_str, stop_type))


def make_network(network_fname):
    """
    Construct a network from an edge list contained in a text file.
    """
    f = open(network_fname)
    edge_list = []
    for line in f:
        result = line.strip().split()
        edge_list.append((int(result[0]), int(result[1])))
    f.close()
    network = nx.Graph()
    network.add_edges_from(edge_list)
    return network
