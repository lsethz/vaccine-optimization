"""
class to simulate a complex epidemic on a network
"""
import networkx as nx
import random
import numpy as np


class Epidemic:
    def __init__(self, threshold, recov_rate):
        """
        Initialize the epidemic object with the viral parameters, and the
        state flags of the nodes.
        """
        # epidemic rates
        self.threshold = threshold
        self.recov_rate = recov_rate

        # Epidemic states
        self.R = 2
        self.I = 1
        self.S = 0

    def init_network_premade(self, net):
        """
        Initialize a premade network structure for an epidemic.
        """
        self.net = net.copy()
        self.node_number = self.net.number_of_nodes()
        self.edges = self.net.number_of_edges()
        self.init_nodes()
        return self.net.copy()

    def init_network(self, node_number, ave_nn, network_seed):
        """Create a ER random nework."""
        self.node_number = node_number
        edge_prob = float(ave_nn) / (node_number - 1)
        self.edge_prob = edge_prob

        self.net = nx.fast_gnp_random_graph(self.node_number, self.edge_prob,
                                            seed=network_seed)

        self.edges = self.net.number_of_edges()

        # Define the initial network, with immunised nodes.
        self.init_nodes()
        return self.net.copy()

    def init_network_small_world(self, node_number, ave_nn,  rewire_prob,
                                 network_seed):
        """Create a small world network."""
        # Network details
        self.node_number = node_number

        self.net = nx.watts_strogatz_graph(self.node_number, ave_nn,
                                           rewire_prob, seed=network_seed)

        self.edges = self.net.number_of_edges()

        # Define the initial network, with immunised nodes.
        self.init_nodes()
        return self.net.copy()

    def init_network_scale_free(self, node_number, ave_nn,  rewire_prob,
                                network_seed):
        """Create a scale-free network."""

        # Network details
        self.node_number = node_number

        self.net = nx.barabasi_albert_graph(self.node_number, ave_nn,
                                            rewire_prob, seed=network_seed)

        self.edges = self.net.number_of_edges()

        # Define the initial network, with immunised nodes.
        self.init_nodes()
        return self.net.copy()

    def init_nodes(self):
        """
        Iterates through each node, gives them a property of state (which could
        be susceptible, infected or recovered), and then sets them all to
        susceptible.

        The network is seeded with infecteds via seed_neighborhood.
        """
        for node in self.net.nodes_iter(data=True):
            # set status of node to be 0
            # first attribute of the node is the index, the second is the
            # dictionary.
            node[1]['state'] = self.S

    def immunise_nodes(self, individual):
        """
        individual is a vector, with states for every node either 0 or 1,
        corresponding to not immunised or immunised respectively. The
        index of the vector relates to the index of a node on the graph, and
        the value, its state. Graph is iniitialized to zero for all nodes
        (susceptible). We pull out the true values, and set those nodes to
        recovered state (2).
        """
        immunise_list = [ind for ind, val in enumerate(list(individual))
                         if val == 1]
        for node_ind in immunise_list:
            self.net.node[node_ind]['state'] = self.R

    def seed_neighborhood(self):
        """
        Complex contagion requires a neighborhood of nodes to be infected
        initially by definition for the contagion to spread. This function
        selects a node at random and infects it and its nearest neighbors.

        TODO: Could select a node where most are immunised? Feature or bug?
        """
        # set the seed to some random time
        random.seed()
        sus_list = self.susceptible_list()
        random_node = random.sample(sus_list, 1)[0]
        nn_list = self.net.neighbors(random_node)
        self.change_state(random_node, 1)
        for n in nn_list:
            if self.get_state(n) == self.S:
                self.change_state(n, self.I)
        # print 'nodes infected: ', nn_list, random_node

    def seed_simple(self):
        """
        Seeds a single random node on the network.
        """
         # set the seed to some random time
        random.seed()
        sus_list = self.susceptible_list()
        random_node = random.sample(sus_list, 1)[0]
        self.change_state(random_node, 1)

    def susceptible_list(self):
        sus_list = []
        for node_ind in self.net.nodes():
            if self.net.node[node_ind]['state'] == self.S:
                sus_list.append(node_ind)
        return sus_list

    def is_node_infected(self, node_ind):
        infected = False
        if self.net.node[node_ind]['state'] == self.I:
            infected = True
        return infected

    def infected_list(self):
        """Return a list of infected nodes"""
        inf_list = []
        for node_ind in self.net.nodes():
            if self.is_node_infected(node_ind):
                inf_list.append(node_ind)
        return inf_list

    def change_state(self, node_ind, new_state):
        self.net.node[node_ind]['state'] = new_state

    def get_state(self, node_ind):
        """Returns state of node"""
        return self.net.node[node_ind]['state']

    def network_snapshot(self):
        return self.net.copy()

    def infected_nn(self, node_ind):
        """
        returns the indexes of the infected nearest neighbors plus the total
        number of nearest neighbors
        """
        nn_list = self.net.neighbors(node_ind)
        infected_list = []
        for n in nn_list:
            if self.is_node_infected(n):
                infected_list.append(n)
        return infected_list, len(nn_list)

    def set_network(self, new_network):
        self.net = new_network

    def get_network(self):
        return self.network_snapshot()

    def reactions(self, complex_contag):
        """
        Iterates through each node on a network and prescribes the possible
        reaction given the state of the noe.
        """
        node_list = self.random_nodes()
        for n in node_list:
            state = self.get_state(n)
            if state == self.S:
                if complex_contag:
                    self.infection_reaction_complex(n)
                else:
                    self.infection_reaction(n)
            elif state == self.I:
                self.recovery_reaction(n)
            elif state == self.R:
                pass
            else:
                print 'In simulation while loop'
                print 'node: ', n
                print 'state: ', state
                raise ValueError('State not recognised!')

    def simulate(self, stop_time, complex_contag, snapshot_times=[]):
        """
        Simulates the spread on an epidemic on a network. Saves the population
        of each state for a given set of snapshot times, up until some stop
        time.
        """
        timer = 0
        current_ind = 0
        sim_snapshots = []
        if len(snapshot_times) != 0:
            if snapshot_times[0] == 0:
                [S, I, R] = self.quick_stats()
                sim_snapshots.append([0, S, I, R])

                current_ind += 1
                current_snapshot_time = snapshot_times[current_ind]

        while timer <= stop_time:
            # select all random nodes
            if self.network_infected():
                self.reactions(complex_contag)

            timer += 1
            # print timer, current_ind, current_snapshot_time
            if len(snapshot_times) != 0:
                if timer == current_snapshot_time:
                    [S, I, R] = self.quick_stats()
                    sim_snapshots.append([timer, S, I, R])
                    if current_ind + 1 < len(snapshot_times):
                        current_ind += 1
                        current_snapshot_time = snapshot_times[current_ind]
                    else:
                        # no more snapshots to be taken
                        pass
        if len(snapshot_times) == 0:
            return self.quick_stats()
        else:
            return np.array(sim_snapshots)

    def simulate_cont(self, complex_contag):
        """
        Simulates the spread of a virus on a network. Runs until there are no
        more infected nodes on the network. Returns the end time, and the
        final state populations.
        """
        t = 0
        while self.network_infected():
            self.reactions(complex_contag)
            t += 1
        return t, self.quick_stats()

    def network_infected(self):
        """
        Checks to see if there is at least one node infected. Otherwise, the
        are no more reactions on the network. Used to speed up simulation.
        """
        still_infected = False
        for node_ind in self.net.nodes():
            if self.net.node[node_ind]['state'] == self.I:
                still_infected = True
                break
            else:
                # node was either susceptible or recovered.
                pass
        return still_infected

    def quick_stats(self):
        """
        returns the absolute numbers of each state on the network.
        """
        S = 0
        I = 0
        R = 0

        for node_ind in xrange(self.node_number):
            state = self.net.node[node_ind]['state']
            if state == self.S:
                S += 1
            elif state == self.I:
                I += 1
            elif state == self.R:
                R += 1
            else:
                print 'node: ', node_ind
                print 'state: ', state
                raise ValueError('Node state not recognised!')
        return [S, I, R]

    def recovery_reaction(self, node_ind):
        """
        Given recovery probability recovery_rate, choose if a node is to
        recover.
        """
        if random.random() <= self.recov_rate:
        # change the state of the node
            self.change_state(node_ind, self.R)

    def infection_reaction_complex(self, node_ind):
        # get nearest neighbors infected
        infected_nns, nn_number = self.infected_nn(node_ind)
        if nn_number != 0:
            # if number of infected equal to or over threshold, infect
            infected_ratio = len(infected_nns) / float(nn_number)
            if infected_ratio >= self.threshold:
                self.change_state(node_ind, self.I)
        else:
            # no nearest neighbors, therefore cannot get infected.
            pass

    def random_nodes(self):
        """
        Selects every node index at random from the network without replacement,
        and returns this list.
        TODO: could speed this up.
        """
        return random.sample(range(self.node_number),
                             len(range(self.node_number)))

    def infection_reaction(self, node_ind):
        # get nearest neighbors infected
        infected_nns, nn_number = self.infected_nn(node_ind)
        if len(infected_nns) != 0:
            to_infect = 1 - np.power((1 - self.threshold), len(infected_nns))
            if to_infect <= random.random():
                self.change_state(node_ind, self.I)

        else:
            # no nearest neighbors, therefore cannot get infected.
            pass
