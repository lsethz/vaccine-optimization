"""
This module is used to run jobs on the Euler cluster
http://clusterwiki.ethz.ch/brutus/Getting_started_with_Euler


Install the relevant modules

>> module load new gcc/4.8.2 python/2.7.9

To submit a job

>> bsub -n 24 -W 24:00 python cluster.py


First is the number of cores, second is the wall time in hh:mm and the rest
is the command for the program.
"""
import vaccine_simulations_FB_mp as vs
import time
import numpy as np


def main():
    thresh_list = [0.42]
    recov_list = [0.167]
    # vaccine_lim_list = [0.1, 0.25, 0.5]
    vaccine_lim_list = list(np.linspace(0.1, 0.9, 9))
    stop_time = -1
    # Genetic Algorithm Parameters.
    crossover = 0.8
    gens = 20
    # number of genereations where the best fitness does not change.
    converge_num = 4

    individ_num = 50
    top_fit = 25
    tourn_size = 10
    ensem_size = 50

    # Genetic Algorithm Parameters.
    ga_params = [individ_num, crossover, tourn_size, top_fit,
                 ensem_size, gens, converge_num]

    db_filename = 'TestSimpleContagion_OnDeck.db'

    network_fname = 'facebook_combined.txt'
    create_db = True
    start_t = time.time()
    vs.parameter_scan_grid(3, network_fname, thresh_list, recov_list,
                           0, vaccine_lim_list, stop_time, ga_params,
                           db_filename, create_db)

    del_t = time.time() - start_t
    print 'Time taken: ', del_t


if __name__ == "__main__":
    main()
