"""
Vaccinate module: takes in a graph and the number of vaccines and prduces
graph genes

This module houses the Genetic Algorithm heuristic. Takes in a contact network
and the number of vaccines, from which can produce random initial genes
(approximate optimal solutions), or conduct tournaments to find the next
generation of genes/solutions. NB: Gene, solution, individual are here
interchangeably synonymous.

For more information, see:
https://en.wikipedia.org/wiki/Genetic_algorithm
"""
import numpy as np
import random


def rand_individuals(node_num, vaccine_limit, individ_num):
    """
    Takes in a network, produces individ_num random graph genes, with
    the number of vaccinated nodes is vaccine_limit.
    """

    # get the node indexes.
    individuals = []
    for ind in xrange(individ_num):
        indy = generate_rand_individual(node_num, vaccine_limit)
        individuals.append(indy)
    return individuals


def generate_rand_individual(node_num, vaccine_limit):
    """
    Generate a random individual given the network.
    """
    vaccine_num = int(vaccine_limit * node_num)
    targets = random.sample(range(node_num), vaccine_num)
    genes = np.zeros((node_num))
    for t in targets:
        genes[t] = 1
    return genes


def mate(father, mother, vaccine_limit, crossover):
    """
    Take in a father individual and mother individual, produce offspring
    with a given crossover percentage (how much genetic info to take from
    the paternal gene).
    """
    vaccine_num = int(vaccine_limit * len(father))
    child = np.zeros((len(father)))

    for ind in xrange(len(father)):
        r = random.random()
        if r <= crossover:
            # Take gene from father
            child[ind] = father[ind]
        else:
            # take gene from mother
            child[ind] = mother[ind]
    # Double check the current child does not immunise initially infected.
    # check sum
    vac_diff = vaccine_num - np.sum(child)
    if vac_diff > 0:
        # add some vaccs.
        susceptibles = [ind for ind, val in enumerate(child) if val == 0]
        sus_ind = random.sample(susceptibles, int(vac_diff))
        for s in sus_ind:
            child[s] = 1

    elif vac_diff < 0:
        # remove some vaccs
        vaccinated = [ind for ind, val in enumerate(child) if val == 1]
        immune_ind = random.sample(vaccinated, int(-1 * vac_diff))
        for i in immune_ind:
            child[i] = 0
    else:
        # vaccine list is good.
        pass
    return child


def new_generation(fitness_arr, top_fittest, vaccine_limit, tournament_size,
                   crossover=0.5):
    """
    Take the top n fittest individuals and pass them immediately on.
    Tournament size dictates who will face who in fitness. Randomly select
    tournament_size individuals, take the top individual as the paternal.
    Then reselect tournament_size individuals (sans paternal) as the
    maternal individual. Mate these two.
    """

    [rows, cols] = np.shape(fitness_arr)
    best_individuals = [fitness_arr[:, c] for c in xrange(top_fittest)]

    # how many individuals must be created through mating.
    children = []
    mating_pool = cols - top_fittest

    for m in xrange(mating_pool):
        father, mother = tournament(fitness_arr, tournament_size)
        child = mate(father, mother, vaccine_limit, crossover)
        children.append(child)
    return best_individuals + children


def tournament(fitness_arr, tournament_size):
    """
    fitness_arr is a matrix whose columns represent the nodes of the matrix,
    and which are immunised. The fittest individual is on the left, descending
    order to the right.
    """
    [rows, cols] = np.shape(fitness_arr)
    tournament_choice = range(cols)
    pat_tournament = random.sample(tournament_choice, tournament_size)
    pat_ind = min(pat_tournament)
    del tournament_choice[pat_ind]
    mat_tournament = random.sample(tournament_choice, tournament_size)
    mat_ind = min(mat_tournament)

    paternal_genes = fitness_arr[:, pat_ind]
    maternal_genes = fitness_arr[:, mat_ind]
    return paternal_genes, maternal_genes
